//
//  PreviewExample.swift
//  PreviewKit
//
//  Created by Logan Radloff on 2/9/20.
//

import SwiftUI

struct PreviewExample: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct PreviewExample_Previews: PreviewProvider {
    static var previews: some View {
//        PreviewExample().preview(PreviewPhone.iPhone11)
//        PreviewExample().previewAlliPhones()
//        PreviewExample().previewAllContentSizes()
//        PreviewExample().previewAllContentSizes(PreviewPhone.iPhoneX)
//        PreviewExample().previewAlliPhones(contentSize: .accessibilityExtraExtraLarge)
//        PreviewExample().previewBaseiPads(contentSize: .accessibilityExtraExtraLarge)
//        PreviewExample().previewAccessibilitySizes.previewLayout(.sizeThatFits)
        PreviewExample().previewBaseiPhones()
    }
}
