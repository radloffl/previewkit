//
//  Preview.swift
//  PreviewKit
//
//  Created by Logan Radloff on 2/9/20.
//

import SwiftUI

public protocol PreviewDeviceConvertible {
    var previewDevice: PreviewDevice { get }
}

// MARK: - View
public extension View {
    //MARK: - Devices
    
    func previewDevices(_ devices: [PreviewDevice]) -> some View {
        Group {
            ForEach(devices, id: \.rawValue) {
                self.preview($0)
            }
        }
    }
    
    func preview(_ device: PreviewDeviceConvertible) -> some View {
        self.preview(PreviewDevice(device))
    }
    
    func preview(_ device: PreviewDevice) -> some View {
        self.previewDisplayName(device.rawValue).previewDevice(device).previewLayout(.device)
    }
    
    // MARK: Mac
    func previewAllMacs() -> some View { self.previewDevices(PreviewDevice.allMacs) }
    func previewBaseMacs() -> some View { self.previewDevices(PreviewDevice.baseMacs) }
    
    // MARK: iPhone
    func previewAlliPhones() -> some View { self.previewDevices(PreviewDevice.alliPhones) }
    func previewBaseiPhones() -> some View { self.previewDevices(PreviewDevice.baseiPhones) }
    
    func previewBaseiPhones(contentSize: ContentSizeCategory) -> some View {
        self.previewBaseiPhones().preview(contentSize)
    }
    
    func previewAlliPhones(contentSize: ContentSizeCategory) -> some View {
        self.previewAlliPhones().preview(contentSize)
    }
    
    // MARK: iPad
    func previewAlliPads() -> some View { self.previewDevices(PreviewDevice.alliPads) }
    func previewBaseiPads() -> some View { self.previewDevices(PreviewDevice.baseiPads) }
    
    func previewBaseiPads(contentSize: ContentSizeCategory) -> some View {
        self.previewBaseiPads().preview(contentSize)
    }
    
    func previewAlliPads(contentSize: ContentSizeCategory) -> some View {
        self.previewAlliPads().preview(contentSize)
    }
    
    // MARK: Apple TV
    func previewAllAppleTVs() -> some View { self.previewDevices(PreviewDevice.allAppleTVs) }
    func previewBaseAppleTVs() -> some View { self.previewDevices(PreviewDevice.baseAppleTVs) }
    
    // MARK: Apple Watch
    func previewAllAppleWatches() -> some View { self.previewDevices(PreviewDevice.allAppleWatches) }
    func previewBaseAppleWatches() -> some View { self.previewDevices(PreviewDevice.baseAppleWatches) }
    
    // MARK: - Accessibility Sizes
    
    func preview(_ contentSize: ContentSizeCategory) -> some View {
        self.environment(\.sizeCategory, contentSize)
    }
    
    func previewContentSizes(contentSizes: [ContentSizeCategory]) -> some View {
        Group {
            ForEach(contentSizes, id: \.hashValue) {
                self.preview($0).previewDisplayName($0.displayName)
            }
        }
    }
    
    func previewAllContentSizes() -> some View { self.previewContentSizes(contentSizes: ContentSizeCategory.allCases) }
    
    func previewAllContentSizes(_ device: PreviewDeviceConvertible) -> some View {
        self.previewAllContentSizes(PreviewDevice(device))
    }
    
    func previewAllContentSizes(_ device: PreviewDevice) -> some View {
        self.previewAllContentSizes().preview(device)
    }
    
    var previewAccessibilitySizes: some View {
        self.previewContentSizes(contentSizes: ContentSizeCategory.accessibilitySizes)
    }
}

// MARK: - PreviewDevice
public extension PreviewDevice {
    init(_ convertible: PreviewDeviceConvertible) {
        self = convertible.previewDevice
    }
    
    static let allMacs = PreviewMac.allCases.toPreviewDevices()
    static let baseMacs = [PreviewMac.mac].toPreviewDevices()
    
    static let alliPhones = PreviewPhone.allCases.map { PreviewDevice(rawValue: $0.rawValue) }
    static let baseiPhones = [PreviewPhone.iPhoneSE, .iPhone8, .iPhone8Plus, .iPhone11, .iPhone11Pro, .iPhone11ProMax].toPreviewDevices()
    
    static let alliPads = PreviewTablet.allCases.map { PreviewDevice(rawValue: $0.rawValue) }
    static let baseiPads = [PreviewTablet.iPadMini4, .iPadAir2, .iPadPro10_5in, .iPadPro12_9in2ndGen, .iPadPro11in, .iPadPro12_9in3rdGen].toPreviewDevices()
    
    static let allAppleTVs = PreviewTV.allCases.map { PreviewDevice(rawValue: $0.rawValue) }
    static let baseAppleTVs = [PreviewTV.AppleTV, .AppleTV4K].toPreviewDevices()
    
    static let allAppleWatches = PreviewWatch.allCases.map { PreviewDevice(rawValue: $0.rawValue) }
    static let baseAppleWatches = [PreviewWatch.AppleWatchSeries3_38mm, .AppleWatchSeries3_42mm, .AppleWatchSeries5_40mm, .AppleWatchSeries5_44mm].toPreviewDevices()
}
