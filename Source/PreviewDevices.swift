//
//  PreviewDevices.swift
//  PreviewKit
//
//  Created by Logan Radloff on 2/9/20.
//

import SwiftUI

// MARK: - Mac
enum PreviewMac: String, CaseIterable, Identifiable, PreviewDeviceConvertible {
    
    case mac = "Mac"
    
    var id: String {
        self.rawValue
    }
    
    var previewDevice: PreviewDevice {
        self.rawValue.toPreviewDevice()
    }
}

// MARK: - Phone
enum PreviewPhone: String, CaseIterable, Identifiable, PreviewDeviceConvertible {
    case iPhone7 = "iPhone 7"
    case iPhone7Plus = "iPhone 7 Plus"
    case iPhone8 = "iPhone 8"
    case iPhone8Plus = "iPhone 8 Plus"
    case iPhoneSE = "iPhone SE"
    case iPhoneX = "iPhone X"
    case iPhoneXs = "iPhone Xs"
    case iPhoneXsMax = "iPhone Xs Max"
    case iPhoneXR = "iPhone Xʀ"
    case iPhone11 = "iPhone 11"
    case iPhone11Pro = "iPhone 11 Pro"
    case iPhone11ProMax = "iPhone 11 Pro Max"
    
    var id: String {
        self.rawValue
    }
    
    var previewDevice: PreviewDevice {
        self.rawValue.toPreviewDevice()
    }
}

// MARK: - Tablet
enum PreviewTablet: String, CaseIterable, Identifiable, PreviewDeviceConvertible {
    case iPadMini4 = "iPad mini 4"
    case iPadAir2 = "iPad Air 2"
    case iPadPro9_7in = "iPad Pro (9.7-inch)"
    case iPadPro12_9in = "iPad Pro (12.9-inch)"
    case iPad5thGen = "iPad (5th generation)"
    case iPadPro12_9in2ndGen = "iPad Pro (12.9-inch) (2nd generation)"
    case iPadPro10_5in = "iPad Pro (10.5-inch)"
    case iPad6thGen = "iPad (6th generation)"
    case iPadPro11in = "iPad Pro (11-inch)"
    case iPadPro12_9in3rdGen = "iPad Pro (12.9-inch) (3rd generation)"
    case iPadMini5thGen = "iPad mini (5th generation)"
    case iPadAir3rdGen = "iPad Air (3rd generation)"
    
    var id: String {
        self.rawValue
    }
    
    var previewDevice: PreviewDevice {
        self.rawValue.toPreviewDevice()
    }
}

// MARK: - TV
enum PreviewTV: String, CaseIterable, Identifiable, PreviewDeviceConvertible {
    case AppleTV = "Apple TV"
    case AppleTV4K = "Apple TV 4K"
    case AppleTV4KAt1080p = "Apple TV 4K (at 1080p)"
    
    var id: String {
        self.rawValue
    }
    
    var previewDevice: PreviewDevice {
        self.rawValue.toPreviewDevice()
    }
}

// MARK: - Watch
enum PreviewWatch: String, CaseIterable, Identifiable, PreviewDeviceConvertible {
    case AppleWatchSeries2_38mm = "Apple Watch Series 2 - 38mm"
    case AppleWatchSeries2_42mm = "Apple Watch Series 2 - 42mm"
    case AppleWatchSeries3_38mm = "Apple Watch Series 3 - 38mm"
    case AppleWatchSeries3_42mm = "Apple Watch Series 3 - 42mm"
    case AppleWatchSeries4_40mm = "Apple Watch Series 4 - 40mm"
    case AppleWatchSeries4_44mm = "Apple Watch Series 4 - 44mm"
    case AppleWatchSeries5_40mm = "Apple Watch Series 5 - 40mm"
    case AppleWatchSeries5_44mm = "Apple Watch Series 5 - 44mm"
    
    var id: String {
        self.rawValue
    }
    
    var previewDevice: PreviewDevice {
        self.rawValue.toPreviewDevice()
    }
}
