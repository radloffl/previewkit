//
//  Preview_Private.swift
//  PreviewKit
//
//  Created by Logan Radloff on 2/9/20.
//

import SwiftUI

internal extension Array where Element: RawRepresentable {
    func toPreviewDevices() -> [PreviewDevice] {
        self.map { ($0.rawValue as! String).toPreviewDevice() }
    }
}

internal extension String {
    func toPreviewDevice() -> PreviewDevice {
        PreviewDevice(rawValue: self)
    }
}

internal extension ContentSizeCategory {
    var displayName: String {
        switch self {
        case .extraSmall:
            return "Extra Small"
        case .small:
            return "Small"
        case .medium:
            return "Medium"
        case .large:
            return "Large"
        case .extraLarge:
            return "Extra Large"
        case .extraExtraLarge:
            return "Extra Extra Large"
        case .extraExtraExtraLarge:
            return "Extra Extra Extra Large"
        case .accessibilityMedium:
            return "Accessibility Medium"
        case .accessibilityLarge:
            return "Accessibility Large"
        case .accessibilityExtraLarge:
            return "Accessibility Extra Large"
        case .accessibilityExtraExtraLarge:
            return "Accessibility Extra Extra Large"
        case .accessibilityExtraExtraExtraLarge:
            return "Accessibility Extra Extra Extra Large"
        @unknown default:
            return "Unknown size"
        }
    }
    
    static var accessibilitySizes = [ContentSizeCategory.accessibilityMedium, .accessibilityLarge, .accessibilityExtraLarge, .accessibilityExtraExtraLarge, .accessibilityExtraExtraExtraLarge]
}
